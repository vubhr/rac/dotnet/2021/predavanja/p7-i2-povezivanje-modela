namespace PovezivanjeModela.Models;

public class Post {
  public int Id { get; set; }
  public string? Title { get; set; }
  public string? Content { get; set; }
  public DateTime PublishedAt { get; set; }

  // dodano nakan sto je stvoren model Category
  public int CategoryId { get; set; }
  
  public Category? Category { get; set; }
}