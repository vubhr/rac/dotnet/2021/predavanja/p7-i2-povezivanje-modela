using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PovezivanjeModela.Models;

    public class PovezivanjeModelaDbContext : DbContext
    {
        public PovezivanjeModelaDbContext (DbContextOptions<PovezivanjeModelaDbContext> options)
            : base(options)
        {
        }

        public DbSet<PovezivanjeModela.Models.Post> Posts { get; set; }

        public DbSet<PovezivanjeModela.Models.Category> Categories { get; set; }
    }
